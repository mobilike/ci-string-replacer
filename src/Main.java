import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public class Main {

  private static String stringKey;
  private static String stringValue;

  public static void main(String[] args) {
    stringKey = args[0];
    stringValue = args[1];

    try {
      Files.walkFileTree(Paths.get("."), new FileVisitor<Path>() {
        @Override
        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
            throws IOException {
          return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
            throws IOException {
          if ("strings.xml".equals(file.getFileName().toString())
              /* && file.toFile().getPath().contains("main") */) {
            modifyXml(file);
            return FileVisitResult.TERMINATE;
          }

          return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult visitFileFailed(Path file, IOException exc)
            throws IOException {
          return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult postVisitDirectory(Path dir, IOException exc)
            throws IOException {
          return FileVisitResult.CONTINUE;
        }
      });
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static void modifyXml(Path file) {
    try {
      Document document = DocumentBuilderFactory.newInstance()
          .newDocumentBuilder()
          .parse(file.toFile());
      document.getDocumentElement().normalize();

      XPathExpression expression = XPathFactory.newInstance()
          .newXPath()
          .compile("//string[@name='" + stringKey + "']");

      Node node = (Node) expression.evaluate(document, XPathConstants.NODE);
      log("Old string value => " + node.getTextContent());
      node.setTextContent(stringValue);
      log("New string value => " + node.getTextContent());

      saveXml(document, file.toFile().getAbsolutePath());
    } catch (SAXException | ParserConfigurationException | XPathExpressionException | IOException e) {
      e.printStackTrace();
    }
  }

  private static void saveXml(Document document, String path) {
    Transformer transformer = null;
    try {
      transformer = TransformerFactory.newInstance().newTransformer();
    } catch (TransformerConfigurationException e) {
      e.printStackTrace();
    }
    Result output = new StreamResult(new File(path));
    Source input = new DOMSource(document);

    try {
      assert transformer != null;
      transformer.transform(input, output);
    } catch (TransformerException e) {
      e.printStackTrace();
    }
  }

  private static void log(String message) {
    System.out.println(message);
  }
}
